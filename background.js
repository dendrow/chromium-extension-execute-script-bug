(function (undefined) {
    'use strict';

    chrome.runtime.onInstalled.addListener(function (details) {
        chrome.contextMenus.create({
            title: 'Test action',
            contexts: ['all'],
            id: 'testAction',
            documentUrlPatterns: [
                'http://*/*',
                'https://*/*',
                'ftp://*/*'
            ]
        });
    });

    chrome.runtime.onMessage.addListener(function (message) {
        console.info(message);
    });

    chrome.contextMenus.onClicked.addListener(function (info, tab) {
        if (contextMenuHandlers.hasOwnProperty(info.menuItemId)) {
            contextMenuHandlers[info.menuItemId](info, tab);
        }
    });

    var contextMenuHandlers = {
        testAction: function (info, tab) {
            console.group('menu handler');
            console.log('testAction before inject %s', tab.id);

            chrome.tabs.executeScript(tab.id, {file: 'inject-code.js'});

            console.log('testAction after inject %s', tab.id);
            console.groupEnd();
        }
    };
})();
