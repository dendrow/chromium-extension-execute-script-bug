# Chromium extension / chrome.tabs.executeScript bug demo #

After downloading any file chrome.tabs.executeScript stops working

Step to reproduce.

1. Open any website
2. Context menu -> "Test action". Work fine. (code executed and message received)
3. Download any file from page
4. Context menu -> "Test action". Nothing happen

